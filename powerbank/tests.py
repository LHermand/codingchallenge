from django.test import TestCase
import json
import os
from powerbank.tools import get_output

class PowerplantTestCase(TestCase):
    def test_powerplant(self):
        cwd = os.getcwd()
        with open(cwd+"\\powerbank\\testfiles\\payload1.json", 'r') as input_file, open(cwd+'\\powerbank\\testfiles\\response_1.json', 'r') as response_file:
            json_input_file = json.load(input_file)
            json_response = json.load(response_file)
            json_return = get_output(json_input_file)
            self.assertEqual(json_return, json_response)
        with open(cwd+"\\powerbank\\testfiles\\payload2.json", 'r') as input_file, open(cwd+'\\powerbank\\testfiles\\response_2.json', 'r') as response_file:
            json_input_file = json.load(input_file)
            json_response = json.load(response_file)
            json_return = get_output(json_input_file)
            self.assertEqual(json_return, json_response)
        with open(cwd+"\\powerbank\\testfiles\\payload3.json", 'r') as input_file, open(cwd+'\\powerbank\\testfiles\\response_3.json', 'r') as response_file:
            json_input_file = json.load(input_file)
            json_response = json.load(response_file)
            json_return = get_output(json_input_file)
            self.assertEqual(json_return, json_response)