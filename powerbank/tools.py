from powerbank.models import Payload, Powerplant

def build_output(list_of_powerplants, fuel):
    '''Build the json output file according to the sorted list_of_powerplants

    Args:
        list_of_powerplant (list): the list of powerplants sorted
        fuel (int): the fuel to acquire from the powerplants
    '''
    json_return_file = []
    for powerplant in list_of_powerplants:
        if fuel > 0 and fuel - powerplant.pmax > 0: #take full fuel from this powerplant
            json_return_file.append({"name": powerplant.name, "p": powerplant.pmax})
            fuel -= powerplant.pmax
        elif fuel > 0 and fuel - powerplant.pmax <= 0: #last powerplant used
            if fuel < powerplant.pmin:
                json_return_file.append({"name": powerplant.name, "p": powerplant.pmin})
            else :
                json_return_file.append({"name": powerplant.name, "p": fuel})
            fuel = 0
        else:  # fuel == 0
            json_return_file.append({"name": powerplant.name, "p": 0})
    return json_return_file

def sort_powerplant_list(list_of_powerplant,load):
    '''Sort the list of powerplant in the merit-order to activate

    Args:
        list_of_powerplant (list): the list to sort
        load (int): the load to acquire from the powerplants
    '''
    current_load = load
    usable_powerplants = [powerplant for powerplant in list_of_powerplant if powerplant.energy_available() > 0]
    unusable_powerplants = [powerplant for powerplant in list_of_powerplant if powerplant not in usable_powerplants] # powerplants that doesn't produce (ex : no wind for windpark)
    sorted_list_of_powerplant = []
    nbr_usable_left = len(usable_powerplants)
    while (nbr_usable_left > 0 and current_load >0):
        next_PP = min(usable_powerplants, key=lambda Powerplant:Powerplant.price_for_Payload(current_load))
        #the next PP in the merit-order to activate
        sorted_list_of_powerplant.append(next_PP)
        usable_powerplants.remove(next_PP)
        current_load = current_load-next_PP.pmax if current_load-next_PP.pmax > 0 else 0
        nbr_usable_left-=1
    for powerplant_left in usable_powerplants+unusable_powerplants:
        # addition in end of list the powerplants that won't be used in no particular order
        sorted_list_of_powerplant.append(powerplant_left)
    return sorted_list_of_powerplant

def get_output(json_input_file):
    '''Build and return the json return file

    Args:
        json_input_file (json): the input file
    '''
    payload = Payload.create_from_json(json_input_file)
    powerplants_before_sorting = []
    initial_load = payload.load
    for powerplant in json_input_file["powerplants"]:
        powerplants_before_sorting.append(Powerplant.create_from_json(powerplant, payload))
    list_of_powerplant = sort_powerplant_list(powerplants_before_sorting, initial_load) # Sort the list of powerplant in the merit-order to activate
    json_return = build_output(list_of_powerplant, initial_load)
    return json_return