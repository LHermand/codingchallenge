import json
import pdb

from django.http import JsonResponse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from powerbank.tools import get_output


@csrf_exempt
def productionplan(request):
    if request.method == "POST":
        try :
            json_input_file = json.loads(request.body)
            json_return = get_output(json_input_file)
            return JsonResponse(json_return, safe=False)
        except :
            return HttpResponse("Invalid input\n Please check for any abnormalities", content_type="application/json")
    else:
        return HttpResponse("Unautorised", content_type="application/json")