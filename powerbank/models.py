from django.db import models


class Fuel(models.Model):
    gas = models.FloatField("gas(euro/MWh)")
    kerosine = models.FloatField("kerosine(euro/MWh)")
    co2 = models.FloatField("co2(euro/ton)")
    wind = models.FloatField("wind(%)")

    @classmethod
    def create_from_json(self, json_dict):
        '''Build the Fuel Object according to json_dict

        Args:
            json_dict (dict): the dict with the values of the object
        '''
        try:
            return self(
                gas=json_dict["gas(euro/MWh)"],
                kerosine=json_dict["kerosine(euro/MWh)"],
                co2=json_dict["co2(euro/ton)"],
                wind=json_dict["wind(%)"],
            )
        except:
            print("Error, Impossible Operation, problem in the fuel json converter")


class Payload(models.Model):
    load = models.IntegerField()
    Fuel = models.ForeignKey(Fuel, on_delete=models.CASCADE)

    @classmethod
    def create_from_json(self, json_dict):
        '''Build the Payload Object according to json_dict

        Args:
            json_dict (dict): the dict with the values of the object
        '''
        try:
            return self(
                load=json_dict["load"], Fuel=Fuel.create_from_json(json_dict["fuels"])
            )
        except:
            print("Error, Impossible Operation, problem in the payload json converter")


class Powerplant(models.Model):
    name = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    efficiency = models.FloatField()
    pmax = models.FloatField()
    pmin = models.FloatField(default=0)
    payload = models.ForeignKey(Payload, on_delete=models.CASCADE)
    
    @classmethod
    def create_from_json(self, json_dict, payload):
        '''Build the Powerplant Object according to json_dict

        Args:
            json_dict (dict): the dict with the values of the Powerplant
        '''
        try:
            return self(
                name=json_dict["name"],
                type=json_dict["type"],
                efficiency=json_dict["efficiency"],
                pmax=json_dict["pmax"],
                pmin=json_dict["pmin"],
                payload=payload,
            )
        except:
            print(
                "Error, Impossible Operation problem in the Powerplant json converter"
            )

    def price_for_launch(self):
        '''
        return the price to launch the powerplant
        '''
        if self.type == "windturbine":
            return 0
        elif self.type == "gasfired":
            return (self.payload.Fuel.gas * self.efficiency + 0.3 * self.payload.Fuel.co2)*self.pmin
        else:
            return (self.payload.Fuel.kerosine * self.efficiency + 0.3 * self.payload.Fuel.co2)*self.pmin

    def energy_available(self):
        '''
        return the maximum amount of energy available with this powerplant
        '''
        if self.type == "windturbine":
            return self.payload.Fuel.wind * self.pmax
        elif self.type == "gasfired":
            return self.efficiency * self.payload.Fuel.gas * self.pmax
        else:
            return self.efficiency * self.payload.Fuel.kerosine * self.pmax

    def price_for_Payload(self):
        '''
        return the price per MWh without conssideration of the price of the pmin
        '''
        if self.type == "windturbine":
            return 0
        elif self.type == "gasfired":
            return self.payload.Fuel.gas * self.efficiency + 0.3 * self.payload.Fuel.co2
        else:
            return self.payload.Fuel.kerosine * self.efficiency + 0.3 * self.payload.Fuel.co2

    def price_for_Payload(self, energy_needed):
        '''
        return the price per MWh taking into conssideration every parameters (pmin, co2)

        Args : 
            energy_needed (int) : the energy left to acquire

        '''
        if self.type == "windturbine":
            return 0
        elif self.type == "gasfired":
            if energy_needed < self.pmin :
                full_price = (self.payload.Fuel.gas * self.efficiency + 0.3 * self.payload.Fuel.co2)*self.pmin
                return full_price/energy_needed
            else :
                return self.payload.Fuel.gas * self.efficiency + 0.3 * self.payload.Fuel.co2
        else:
            if energy_needed < self.pmin :
                full_price = (self.payload.Fuel.kerosine * self.efficiency + 0.3 * self.payload.Fuel.co2)*self.pmin
                return full_price/energy_needed
            else :
                return self.payload.Fuel.kerosine * self.efficiency + 0.3 * self.payload.Fuel.co2

    
